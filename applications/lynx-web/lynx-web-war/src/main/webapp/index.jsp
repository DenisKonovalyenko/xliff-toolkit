<%@ page import="net.sf.okapi.lib.xliff2.Const" %>
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <title>Okapi Lynx-Web</title>
    <style type="text/css">body { font-family: Arial, Helvetica, sans-serif; }</style>
  </head>
  <body>
    <h1>Okapi Lynx-Web</h1>
	<p>Validation and Testing Tool for XLIFF 2</p>
    <ul>
	  <!-- <li>Host Address: <%=java.net.InetAddress.getLocalHost().getHostAddress()%></li> -->
      <li>XLIFF Toolkit Project: <a target='_blank' href='https://bitbucket.org/okapiframework/xliff-toolkit'>https://bitbucket.org/okapiframework/xliff-toolkit</a></li>
      <li>XLIFF Library version: 1.1.9-SNAPSHOT <!-- <%=net.sf.okapi.lib.xliff2.Const.class.getPackage().getImplementationVersion()%> --></li>
    </ul>
    <h3>Services Available</h3>
    <ul>
      <li><a href="/validation">Validation</a></li>
      <li><a href="/fragments">Fragment Identification</a></li>
    </ul>
  </body>
</html>
