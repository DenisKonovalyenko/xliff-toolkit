<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>net.sf.okapi</groupId>
	<artifactId>xlifftk-build-okapi</artifactId>
	<version>1.1.9-SNAPSHOT</version>

	<packaging>pom</packaging>
	<name>Okapi XLIFF Toolkit</name>
	<description>The Okapi Framework is a cross-platform and free open-source
		set of components and applications that offer extensive support for
		localizing and translating documentation and software.</description>
	<url>https://bitbucket.org/okapiframework/xliff-toolkit</url>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<issueManagement>
		<system>Bitbucket</system>
		<url>https://bitbucket.org/okapiframework/xliff-toolkit/issues</url>
	</issueManagement>

	<developers>
		<developer>
			<name>Okapi Developer Team</name>
			<email>contact@okapiframework.org</email>
			<organization>None</organization>
			<organizationUrl>https://okapiframework.org</organizationUrl>
		</developer>
	</developers>

	<scm>
		<connection>scm:git:https://bitbucket.org/okapiframework/xliff-toolkit.git</connection>
		<developerConnection>scm:git:https://bitbucket.org/okapiframework/xliff-toolkit</developerConnection>
		<url>https://bitbucket.org/okapiframework/xliff-toolkit/src</url>
	</scm>

	<distributionManagement>
		<repository>
			<id>sonatype</id>
			<name>Sonatype Releases</name>
			<url>https://oss.sonatype.org/service/local/staging/deploy/maven2</url>
		</repository>
		<snapshotRepository>
			<id>sonatype</id>
			<name>Sonatype Snapshots</name>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
	</distributionManagement>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.deploy.skip>false</maven.deploy.skip>
	</properties>

	<modules>
		<module>libraries</module>
	</modules>
	<dependencies>
		<!--
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.2</version>
		</dependency>
		-->
		<!-- For tests -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<!--<plugin> <groupId>org.apache.maven.plugins</groupId> <artifactId>maven-surefire-plugin</artifactId> 
				<version>2.6</version> <configuration> <parallel>classes</parallel> <useUnlimitedThreads>true</useUnlimitedThreads> 
				</configuration> </plugin> -->

			<plugin>
				<groupId>org.apache.felix</groupId>
				<version>3.5.1</version>
				<artifactId>maven-bundle-plugin</artifactId>
				<extensions>true</extensions>
				<executions>
					<execution>
						<id>bundle-manifest</id>
						<phase>process-classes</phase>
						<goals>
							<goal>manifest</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<manifestLocation>${basedir}/META-INF</manifestLocation>
					<supportedProjectTypes>
						<supportedProjectType>jar</supportedProjectType>
						<supportedProjectType>bundle</supportedProjectType>
						<supportedProjectType>war</supportedProjectType>
					</supportedProjectTypes>					
				</configuration>
			</plugin>

			<!-- <plugin>
				<artifactId>maven-jar-plugin</artifactId>
				<version>2.3.1</version>
				<configuration>
					<archive>					
						<manifestFile>${project.build.outputDirectory}/META-INF/MANIFEST.MF</manifestFile>
						<manifestFile>${basedir}/META-INF/MANIFEST.MF</manifestFile>
					</archive>
				</configuration>
			</plugin> -->

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.1.0</version>
				<executions>
					<execution>
						<goals>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<profiles>
		<profile>
			<id>sign_and_deploy</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<version>1.5</version>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<skip>false</skip>
							<lockMode>never</lockMode>
							<defaultKeyring>false</defaultKeyring>
							<useAgent>false</useAgent>
							<gpgArguments>
								<arg>--no-random-seed-file</arg>
								<arg>--no-permission-warning</arg>
							</gpgArguments>
						</configuration>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-deploy-plugin</artifactId>
						<configuration>
							<skip>true</skip>
						</configuration>
					</plugin>
					<plugin>
						<groupId>org.sonatype.plugins</groupId>
						<artifactId>nexus-staging-maven-plugin</artifactId>
						<version>1.6.8</version>
						<extensions>true</extensions>
						<executions>
							<execution>
								<id>default-deploy</id>
								<phase>deploy</phase>
								<goals>
									<goal>deploy</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<serverId>sonatype</serverId>
							<nexusUrl>https://oss.sonatype.org/</nexusUrl>
							<!-- Set this to true and the release will automatically proceed and sync to Central Repository will follow  -->
							<autoReleaseAfterClose>false</autoReleaseAfterClose>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>release</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-source-plugin</artifactId>
						<version>3.0.1</version>
						<executions>
							<execution>
								<id>attach-sources</id>
								<goals>
									<goal>jar-no-fork</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
						<version>3.0.0</version>
						<configuration>
							<doclint>none</doclint>
						</configuration>
						<executions>
							<execution>
								<id>attach-javadocs</id>
								<goals>
									<goal>jar</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
