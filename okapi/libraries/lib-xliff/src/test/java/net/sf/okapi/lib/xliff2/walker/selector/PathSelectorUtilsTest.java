/*===========================================================================
  Copyright (C) 2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.xliff2.walker.selector;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Class for testing of path selector utils.
 *
 * @author Vladyslav Mykhalets
 */
public class PathSelectorUtilsTest {

    @Test
    public void testPathSelector() {

        XliffWalkerPathSelector[] pathSelectors = new XliffWalkerPathSelector.Builder()
                .selector("f1", "tu1")
                .selector("f1", "tu2", 0)
                .selector("f1", "tu3", 1)
                .build();

        assertEquals(1, pathSelectors.length);

        boolean containsFile = PathSelectorUtils.containsFile(pathSelectors[0], "f1");
        assertEquals(true, containsFile);

        containsFile = PathSelectorUtils.containsFile(pathSelectors[0], "f2");
        assertEquals(false, containsFile);

        boolean containsUnit = PathSelectorUtils.containsUnit(pathSelectors[0], "f1", "tu1");
        assertEquals(true, containsUnit);

        containsUnit = PathSelectorUtils.containsUnit(pathSelectors[0], "f1", "tu2");
        assertEquals(true, containsUnit);

        containsUnit = PathSelectorUtils.containsUnit(pathSelectors[0], "f1", "tu3");
        assertEquals(true, containsUnit);

        boolean containsSegment = PathSelectorUtils.containsSegment(pathSelectors[0], "f1", "tu1", 0);
        assertEquals(true, containsSegment);

        containsSegment = PathSelectorUtils.containsSegment(pathSelectors[0], "f1", "tu1", 1);
        assertEquals(true, containsSegment);

        containsSegment = PathSelectorUtils.containsSegment(pathSelectors[0], "f1", "tu2", 0);
        assertEquals(true, containsSegment);

        containsSegment = PathSelectorUtils.containsSegment(pathSelectors[0], "f1", "tu2", 1);
        assertEquals(false, containsSegment);

        containsSegment = PathSelectorUtils.containsSegment(pathSelectors[0], "f1", "tu3", 1);
        assertEquals(true, containsSegment);

        containsSegment = PathSelectorUtils.containsSegment(pathSelectors[0], "f1", "tu3", 0);
        assertEquals(false, containsSegment);
    }

}
